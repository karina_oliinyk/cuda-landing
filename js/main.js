(function() {

const pageNavigatorWidget = `
  <div class="bookmark">
    <header class="bookmark__header bm-header">
      <p class="bm-header__title">Search node element</p>
      <div class="bm-header__close">X</div>
    </header>
      <div class="bookmark__search bm-search">
        <input type="text" class="bm-search__input">
        <input class="bm-search__button" value="Search" type="submit">
      </div>
      <nav class="bookmark__navigator bm-navigator">
        <button class="bm-navigator__prev bm-btn">Prev</button>
        <button class="bm-navigator__next bm-btn">Next</button>
        <button class="bm-navigator__parent bm-btn">Parent</button>
        <button class="bm-navigator__children bm-btn">Children</button>
      </nav>
  </div>
`;

const shadowWidget = document.createElement('div');
const shadow = shadowWidget.attachShadow({ mode:'open' });
shadow.innerHTML = pageNavigatorWidget;
document.body.appendChild(shadowWidget); 

shadowWidget.shadowRoot.querySelector('.bookmark').style = `
  background: rgba(0,0,0,0.7);
  color: rgba(0, 0, 0, 1);
  font-family: Bradley Hand, cursive;
  position: fixed;
  z-index: 1000;
  left: 10px;
  top: 10px;
`;

shadowWidget.shadowRoot.querySelector('.bm-header').style = `
  background: rgba(0,0,0,0.2);
  color: #fff;
  display: flex;
  font-size: 20px;
  font-weight: 900;
  justify-content: space-between;
  letter-spacing: 2px;
  padding: 10px;
`;

shadowWidget.shadowRoot.querySelector('.bm-header__title').style = `
  margin: 0;
`;

shadowWidget.shadowRoot.querySelector('.bm-header__close').style = `
  background: rgba(0, 0, 0, 0);
  border: none;
  color: #fff;
  font-size: 19px;
  cursor: pointer;
`;

shadowWidget.shadowRoot.querySelector('.bm-search__input').style = `
  font-size: 18px;
  padding: 0 5px;
  width: 65%;
`;

shadowWidget.shadowRoot.querySelectorAll('.bm-search__button, .bm-btn').forEach(Element => {
  Element.style = `
    width: 24%;
    font-size: 16px;`;
});

shadowWidget.shadowRoot.querySelectorAll('.bm-search, .bm-navigator').forEach(Element => {
  Element.style = `
  display: flex;
  justify-content: space-between;
  padding: 0 15px;
  margin-bottom: 10px`;
});

const widget = shadowWidget.shadowRoot.querySelector('.bookmark');
const widgetHeader = shadowWidget.shadowRoot.querySelector('.bookmark__header');
const closeWidget = shadowWidget.shadowRoot.querySelector('.bm-header__close');
const searchInput = shadowWidget.shadowRoot.querySelector('.bm-search__input');
const searchButton = shadowWidget.shadowRoot.querySelector('.bm-search__button');
const buttonNav = shadowWidget.shadowRoot.querySelector('.bm-btn');
const buttonPrev = shadowWidget.shadowRoot.querySelector('.bm-navigator__prev');
const buttonNext = shadowWidget.shadowRoot.querySelector('.bm-navigator__next');
const buttonParent = shadowWidget.shadowRoot.querySelector('.bm-navigator__parent');
const buttonChildren = shadowWidget.shadowRoot.querySelector('.bm-navigator__children');
let currentElement;

function defineRelation() {
  buttonPrev.disabled = false;
  buttonNext.disabled = false;
  buttonParent.disabled = false;
  buttonChildren.disabled = false;

  if (!currentElement.previousElementSibling && !currentElement.nextElementSibling) {
    buttonPrev.disabled = true;
    buttonNext.disabled = true;
  } else if (!currentElement.previousElementSibling) {
    buttonPrev.disabled = true;
  } 
  else if (!currentElement.nextElementSibling) {
    buttonNext.disabled = true;
  }

  if(!currentElement.parentElement && !currentElement.firstElementChild) {
    buttonParent.disabled = true;
    buttonChildren.disabled = true;
  } else if (!currentElement.parentElement) {
    buttonParent.disabled = true;
  } else if (!currentElement.firstElementChild) {
    buttonChildren.disabled = true;
  }
};

function disableButton() {
  if (buttonNav.disabled == true) {
    buttonNav.disabled = false;
  }     
};

function showElement() {
  currentElement.scrollIntoView( {block: 'center'} );
  currentElement.style = `border: 2px solid #f00`;
  searchInput.value = currentElement.tagName.toLowerCase();
};

closeWidget.addEventListener('click', () => {
  widget.remove();
});

searchButton.addEventListener('click', () => {
  if (currentElement !== undefined) {
    currentElement.style = `border: none`;
  }

  currentElement = document.querySelector(searchInput.value); 
  console.dir(currentElement);
  showElement();
  disableButton();
  defineRelation();
});

buttonPrev.addEventListener('click', () => {
  currentElement.style = `border: none`;
  currentElement = currentElement.previousElementSibling;
  showElement();
  disableButton();
  defineRelation();
});

buttonNext.addEventListener('click', () => { 
  currentElement.style = `border: none`;
  currentElement = currentElement.nextElementSibling;
  showElement();
  disableButton();
  defineRelation();
});

buttonParent.addEventListener('click', () => {
  currentElement.style = `border: none`;
  currentElement = currentElement.parentElement;
  showElement();
  disableButton();
  defineRelation();
});

buttonChildren.addEventListener('click', () => {
  currentElement.style = `border: none`;
  currentElement = currentElement.firstElementChild;
  showElement();
  disableButton();
  defineRelation();
});

widgetHeader.onmousedown = function(event) {
  let shiftX = event.pageX - (widgetHeader.getBoundingClientRect().left + pageXOffset);
  let shiftY = event.pageY - (widgetHeader.getBoundingClientRect().top + pageYOffset)

  widget.style.position = 'absolute';
  moveAt(event.pageX, event.pageY);

  function moveAt(pageX, pageY) {
    widget.style.left = pageX - shiftX + 'px';
    widget.style.top = pageY - shiftY + 'px';
  }

  function onmousemove(event) {
    moveAt(event.pageX, event.pageY);
  }

  document.addEventListener('mousemove', onmousemove);

  widgetHeader.onmouseup = function(stopMove) {
    document.removeEventListener('mousemove', onmousemove);
    widgetHeader.onmouseup = null;
    widget.style.top = stopMove.clientY - shiftY + 'px';
    widget.style.position = 'fixed';
  };
};

widgetHeader.ondragstart = function() {
  return false;
};

})()

// !function(){const e=document.createElement("div");e.attachShadow({mode:"open"}).innerHTML='\n  <div class="bookmark">\n    <header class="bookmark__header bm-header">\n      <p class="bm-header__title">Search node element</p>\n      <div class="bm-header__close">X</div>\n    </header>\n      <div class="bookmark__search bm-search">\n        <input type="text" class="bm-search__input">\n        <input class="bm-search__button" value="Search" type="submit">\n      </div>\n      <nav class="bookmark__navigator bm-navigator">\n        <button class="bm-navigator__prev bm-btn">Prev</button>\n        <button class="bm-navigator__next bm-btn">Next</button>\n        <button class="bm-navigator__parent bm-btn">Parent</button>\n        <button class="bm-navigator__children bm-btn">Children</button>\n      </nav>\n  </div>\n',document.body.appendChild(e),e.shadowRoot.querySelector(".bookmark").style="\n  background: rgba(0,0,0,0.7);\n  color: rgba(0, 0, 0, 1);\n  font-family: Bradley Hand, cursive;\n  position: fixed;\n  z-index: 1000;\n  left: 10px;\n  top: 10px;\n",e.shadowRoot.querySelector(".bm-header").style="\n  background: rgba(0,0,0,0.2);\n  color: #fff;\n  display: flex;\n  font-size: 20px;\n  font-weight: 900;\n  justify-content: space-between;\n  letter-spacing: 2px;\n  padding: 10px;\n",e.shadowRoot.querySelector(".bm-header__title").style="\n  margin: 0;\n",e.shadowRoot.querySelector(".bm-header__close").style="\n  background: rgba(0, 0, 0, 0);\n  border: none;\n  color: #fff;\n  font-size: 19px;\n  cursor: pointer;\n",e.shadowRoot.querySelector(".bm-search__input").style="\n  font-size: 18px;\n  padding: 0 5px;\n  width: 65%;\n",e.shadowRoot.querySelectorAll(".bm-search__button, .bm-btn").forEach(e=>{e.style="\n    width: 24%;\n    font-size: 16px;"}),e.shadowRoot.querySelectorAll(".bm-search, .bm-navigator").forEach(e=>{e.style="\n  display: flex;\n  justify-content: space-between;\n  padding: 0 15px;\n  margin-bottom: 10px"});const n=e.shadowRoot.querySelector(".bookmark"),t=e.shadowRoot.querySelector(".bookmark__header"),o=e.shadowRoot.querySelector(".bm-header__close"),a=e.shadowRoot.querySelector(".bm-search__input"),r=e.shadowRoot.querySelector(".bm-search__button"),l=e.shadowRoot.querySelector(".bm-btn"),s=e.shadowRoot.querySelector(".bm-navigator__prev"),i=e.shadowRoot.querySelector(".bm-navigator__next"),d=e.shadowRoot.querySelector(".bm-navigator__parent"),c=e.shadowRoot.querySelector(".bm-navigator__children");let b;function u(){s.disabled=!1,i.disabled=!1,d.disabled=!1,c.disabled=!1,b.previousElementSibling||b.nextElementSibling?b.previousElementSibling?b.nextElementSibling||(i.disabled=!0):s.disabled=!0:(s.disabled=!0,i.disabled=!0),b.parentElement||b.firstElementChild?b.parentElement?b.firstElementChild||(c.disabled=!0):d.disabled=!0:(d.disabled=!0,c.disabled=!0)}function m(){1==l.disabled&&(l.disabled=!1)}function p(){b.scrollIntoView({block:"center"}),b.style="border: 2px solid #f00",a.value=b.tagName.toLowerCase()}o.addEventListener("click",()=>{n.remove()}),r.addEventListener("click",()=>{void 0!==b&&(b.style="border: none"),b=document.querySelector(a.value),console.dir(b),p(),m(),u()}),s.addEventListener("click",()=>{b.style="border: none",b=b.previousElementSibling,p(),m(),u()}),i.addEventListener("click",()=>{b.style="border: none",b=b.nextElementSibling,p(),m(),u()}),d.addEventListener("click",()=>{b.style="border: none",b=b.parentElement,p(),m(),u()}),c.addEventListener("click",()=>{b.style="border: none",b=b.firstElementChild,p(),m(),u()}),t.onmousedown=function(e){let o=e.pageX-(t.getBoundingClientRect().left+pageXOffset),a=e.pageY-(t.getBoundingClientRect().top+pageYOffset);function r(e,t){n.style.left=e-o+"px",n.style.top=t-a+"px"}function l(e){r(e.pageX,e.pageY)}n.style.position="absolute",r(e.pageX,e.pageY),document.addEventListener("mousemove",l),t.onmouseup=function(e){document.removeEventListener("mousemove",l),t.onmouseup=null,n.style.top=e.clientY-a+"px",n.style.position="fixed"}},t.ondragstart=function(){return!1}}();